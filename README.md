# Thesis
Run `build.sh` on Unix systems and `build.bat` on Windows systems to automatically generate `thesis.pdf` whenever the source files are updated. It uses `latexmk`, `pdflatex`, `biblatex` and `biber` to generate the PDF file.

The subfolder `chapters` may be used to organize the source files. The subfolder `data` may be used to keep files, that are not needed for the actual PDF file generation. Git will treat them as binary files. (no diffs and no normalization of line endings - think PDFs, ODT files and the like)

## Requirements
The template relies on a proper TeX distribution, Perl and was tested on Windows 10 and Ubuntu 14.04, but should work on a lot of systems. It is suggested to use Sublime Text 3 and Git, but that is not a requirement.

| Unix | Windows |
| :--- | :------ |
| [TeX Live](https://www.tug.org/texlive/debian.html)  | [MiKTeX](http://miktex.org/howto/install-miktex)  |
| -refer to your distribution-  | [ActivePerl](https://www.activestate.com/activeperl/downloads) |

## PDF Viewer
The script tries to detect Windows and then tries to use [Evince](https://wiki.gnome.org/Apps/Evince/Downloads):
```
$pdf_previewer = "\"C:\\Program Files (x86)\\Evince-2.32.0.145\\bin\\evince.exe\"";
```

If Windows is not detected `xdg-open` is used to display the generated PDF file automatically:
```
$pdf_previewer = "xdg-open";
```

## Spell checking
`spell-check.sh` is (not on Windows) a shorthand for spell checking all TeX files with `aspell`.

## Acknowledgements
The copyright declaration is taken from a form ([Erklaerung_Arbeit.pdf, ~1.0 MB](http://www.uni-bremen.de/fileadmin/user_upload/single_sites/zpa/pdf/allgemein/Erklaerung_Arbeit.pdf), version from July 8, 2016) published by the Zentrales Prüfungsamt at University of Bremen.

This template includes "Hinweise für gelungene Abschlussarbeiten" (version 1.5) by Rainer Malaka, Andreas Breiter and the research group Digital Media at University of Bremen.
