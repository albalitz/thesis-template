if ($^O eq 'MSWin32')
{
  $pdf_previewer = "\"C:\\Program Files (x86)\\Evince-2.32.0.145\\bin\\evince.exe\"";
}
else
{
  $pdf_previewer = "xdg-open";
}

$out_dir = 'build';
$preview_continuous_mode = 1;
$pdflatex = 'pdflatex --shell-escape %O -interaction=nonstopmode -synctex=1 %S';
$pdf_mode = 1;
