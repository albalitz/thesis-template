\ProvidesClass{thesis}
\LoadClass[12pt,a4paper,twoside,openright]{report}

\RequirePackage[utf8]{inputenc} % input file encoding
\RequirePackage[T1]{fontenc} % font encoding

\RequirePackage[ngerman]{babel} % language settings
\RequirePackage{csquotes} % better quotation, depending on context and language settings
\RequirePackage{parskip} % exchange paragraph indention for spacing in between
\RequirePackage{hyperref} % clickable links
\RequirePackage{etoolbox} % custom commands helper
\RequirePackage{graphicx} % better graphics
\RequirePackage{geometry} % layouting

%% BIBLIOGRAPHY %%
\RequirePackage[backend=biber,
    sorting=none, % http://tex.stackexchange.com/a/51439
    language=german,
    maxcitenames=2]{biblatex} %% biber with biblatex
\DeclareLanguageMapping{german}{german-apa} % bibliography language settings
\DeclareFieldFormat{apacase}{#1} % disable case altering
\newrobustcmd{\citeright}[1]{\begin{flushright}-- \citeauthor{#1}, \citeyear{#1}\end{flushright}} % like \cite{} but with a dash and aligned to the right

%% TITLEPAGE %%

\newrobustcmd*{\university}[1]{%
  \ifblank{#1}{%
    \gdef\@university{Universität Bremen}%
  }{%
    \gdef\@university{#1}%
  }%
}
\newrobustcmd*{\universitylogo}[1]{\gdef\@universitylogo{#1}%
}
\newrobustcmd*{\faculty}[1]{
  \ifblank{#1}{%
    \gdef\@faculty{Fachbereich 3: Mathematik/Informatik}%
  }{%
    \gdef\@faculty{#1}%
  }%
}
\newrobustcmd*{\subtitle}[1]{%
  \ifblank{#1}{%
    \gdef\@subtitle{}%
  }{%
    \gdef\@subtitle{#1}%
  }
}
\newrobustcmd*{\authorfirstname}[1]{\gdef\@authorfirstname{#1}%
}
\newrobustcmd*{\authorlastname}[1]{\gdef\@authorlastname{#1}%
}
\newrobustcmd*{\studentnumber}[1]{\gdef\@studentnumber{#1}%
}
\newrobustcmd*{\major}[1]{%
  \ifblank{#1}{%
    \gdef\@major{Informatik}%
  }{%
    \gdef\@major{#1}
  }
}
\newrobustcmd*{\project}[1]{%
  \ifblank{#1}{%
    \gdef\@project{Abschlussarbeit}%
  }{%
    \gdef\@project{#1}
  }
}
\newrobustcmd*{\degree}[1]{%
  \ifblank{#1}{%
    \gdef\@degree{Bachelor of Science}%
  }{%
    \gdef\@degree{#1}
  }
}
\newrobustcmd*{\version}[1]{\gdef\@version{#1}%
}
\newrobustcmd*{\supervisor}[1]{
  \gdef\@supervisora{#1}%
}
\newrobustcmd*{\supervisors}[2]{
  \gdef\@supervisora{#1}%
  \gdef\@supervisorb{#2}%
}
\newrobustcmd*{\examiners}[2]{
  \gdef\@examinera{#1}%
  \gdef\@examinerb{#2}%
}

\renewrobustcmd*{\maketitle}{%
\newgeometry{centering}
\begin{titlepage}\centering
\ifdef{\@university}{\@university\par}{}

\ifdef{\@universitylogo}{\@universitylogo\par}{}
\ifdef{\@faculty}{\@faculty\par}{}

\vspace{2.5cm}
\ifdef{\@title}{{\Large\@title}\par}{}
\ifdef{\@subtitle}{{\large\@subtitle}\par}{}

\vspace{2.5cm}
\ifdef{\@authorfirstname}{{\itshape\large\@authorfirstname}}{}
\ifdef{\@authorlastname}{{\itshape\large\@authorlastname}}{}
\par

\vspace{1cm}
\ifdef{\@studentnumber}{Matrikelnummer: \@studentnumber\par}{}
\ifdef{\@major}{Studiengang: {\scshape\@major}\par}{}

\vspace{1.5cm}
\ifdef{\@project}{%
  \ifdef{\@degree}{%
    \@project{} zur Erlangung des akademischen Grades\par%
    {\scshape\@degree}\par%
  }{%
    \@project%
  }%
}{%
  \ifdef{\@degree}{
    Zur Erlagung des akademischen Grades\par%
    {\scshape\@degree}\par%
  }{}%
}

\vfill
\ifdef{\@date}{\@date\par}{}
\ifdef{\@version}{Version: \@version\par}{}

\vspace{1cm}
\begin{tabular}{ r l }
  \ifdef{\@examinera}{Erstprüfer: & {\itshape\@examinera} \\}{}
  \ifdef{\@examinerb}{Zweitprüfer: & {\itshape\@examinerb} \\}{}
  \ifdef{\@supervisora}{Betreuer: & {\itshape\@supervisora} \\}{}
  \ifdef{\@supervisorb}{ & {\itshape\@supervisorb} \\}{}
\end{tabular}

\end{titlepage}
\restoregeometry
}


\newrobustcmd*{\copyrightdeclaration}{%
\cleardoublepage
\noindent
\textbf{Urheberrechtliche Erklärung}
\par

\vspace{1cm}
\noindent
Hiermit versichere ich, dass ich die vorliegende Arbeit selbstständig verfasst und keine anderen als die angegebenen Quellen und Hilfsmittel verwendet habe. Alle Stellen, die ich wörtlich oder sinngemäß aus anderen Werken entnommen habe, habe ich unter Angabe der Quellen als solche kenntlich gemacht.
\par

\vspace{2cm}
\begin{tabular}{ll}
  Nachname & \@authorlastname \\
  \\
  Vorname/n & \@authorfirstname \\
  \\
  Matrikelnummer & \@studentnumber \\
  \\
  Datum & \today \\
  \\
  \\
  \\
  Unterschrift & \line(1,0){230}
\end{tabular}
\par
}
